import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DevExtremeModule, DxButtonModule} from 'devextreme-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {APP_BASE_HREF} from '@angular/common';
import { IndexComponent } from './modules/duongdt/index/index.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxButtonModule,
    DevExtremeModule
  ],
  providers: [{provide: APP_BASE_HREF, useValue : '/' }],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
